# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :publishers
  resources :books
  resources :members
  resources :book_publishers
  resources :book_members
  get "home/index"
  root to: "home#index"
end
