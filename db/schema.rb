# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_30_173506) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "book_members", force: :cascade do |t|
    t.bigint "book_id"
    t.bigint "member_id"
    t.string "issue_date"
    t.string "due_date"
    t.string "return_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["book_id"], name: "index_book_members_on_book_id"
    t.index ["member_id"], name: "index_book_members_on_member_id"
  end

  create_table "book_publishers", force: :cascade do |t|
    t.bigint "book_id"
    t.bigint "publisher_id"
    t.string "pub_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["book_id"], name: "index_book_publishers_on_book_id"
    t.index ["publisher_id"], name: "index_book_publishers_on_publisher_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "author"
    t.string "title"
    t.float "price"
    t.string "availability"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "members", force: :cascade do |t|
    t.text "memb_info"
    t.string "memb_type"
    t.string "address"
    t.string "attriName"
    t.string "expiry_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "publishers", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
