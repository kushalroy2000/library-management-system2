class CreateBookPublishers < ActiveRecord::Migration[6.0]
  def change
    create_table :book_publishers do |t|
      t.belongs_to :book
      t.belongs_to :publisher
      t.string :pub_date

      t.timestamps
    end
  end
end
