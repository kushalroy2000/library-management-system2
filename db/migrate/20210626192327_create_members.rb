# frozen_string_literal: true

class CreateMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :members do |t|
      t.text :memb_info
      t.string :memb_type
      t.string :address
      t.string :attriName
      t.string :expiry_date

      t.timestamps
    end
  end
end
