class CreateBookMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :book_members do |t|
      t.belongs_to :book
      t.belongs_to :member
      t.string :issue_date
      t.string :due_date
      t.string :return_date

      t.timestamps
    end
  end
end
