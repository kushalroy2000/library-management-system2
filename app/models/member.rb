# frozen_string_literal: true

# == Schema Information
#
# Table name: members
#
#  id          :bigint           not null, primary key
#  memb_info   :text
#  memb_type   :string
#  address     :string
#  attriName   :string
#  expiry_date :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Member < ApplicationRecord
  validates :memb_info, :memb_type, :address, :attriName, :expiry_date, presence: true

  has_many :book_members
  has_many :books, through: :book_members
end
