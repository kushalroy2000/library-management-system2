# == Schema Information
#
# Table name: book_publishers
#
#  id           :bigint           not null, primary key
#  book_id      :bigint
#  publisher_id :bigint
#  pub_date     :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class BookPublisher < ApplicationRecord
    validates :pub_date, presence: true
    
    belongs_to :book
    belongs_to :publisher
end
