# == Schema Information
#
# Table name: book_members
#
#  id          :bigint           not null, primary key
#  book_id     :bigint
#  member_id   :bigint
#  issue_date  :string
#  due_date    :string
#  return_date :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class BookMember < ApplicationRecord
    validates :issue_date, :due_date, presence: true

    belongs_to :book
    belongs_to :member
end
