# frozen_string_literal: true

# == Schema Information
#
# Table name: books
#
#  id           :bigint           not null, primary key
#  author       :string
#  title        :string
#  price        :float
#  availability :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Book < ApplicationRecord
  validates :author, :title, :price, :availability, presence: true

  has_one :book_publisher
  has_one :publisher, through: :book_publisher
  has_many :book_members
  has_many :members, through: :book_members
end
