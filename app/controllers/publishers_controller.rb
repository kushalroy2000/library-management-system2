# frozen_string_literal: true

class PublishersController < ApplicationController
  def new; end

  def create
    @publisher = Publisher.new(publisher_params)
    begin
      if @publisher.save!
        redirect_to action: :index
        flash[:success] = 'Publisher has been saved successfully 🥳 ... '
      end
    rescue StandardError
      flash.now[:danger] = @publisher.errors.full_messages.to_s
      render new_publisher_path, status: :not_acceptable
    end
  end

  def index
    @publisher = Publisher.all
  end

  def show
    @publisher = Publisher.find(params[:id])
  end

  def update
    @publisher = Publisher.find(params[:id])
    begin
      if @publisher.update!(publisher_params)
        redirect_to action: :index
        flash[:success] = 'Publisher has been updated successfully 🥳 ... '
      end
    rescue StandardError
      flash.now[:danger] = @publisher.errors.full_messages.to_s
      render action: 'edit'
    end
  end

  def edit
    @publisher = Publisher.find(params[:id])
  end

  def destroy
    Publisher.find(params[:id]).destroy
    redirect_to action: :index
    flash[:success] = 'Publisher has been deleted successfully 🥳 ... '
  end

  private

  def publisher_params
    params.require(:publisher).permit(:name, :address)
  end
end
