class BookPublishersController < ApplicationController
    def new
        @books = Book.all
        @publishers = Publisher.all
    end

    def create
      @book_publisher = BookPublisher.new(bookp_params)
      begin
        if @book_publisher.save!
          redirect_to action: :index
          flash[:success] = 'BookPublisher has been saved successfully 🥳 ... '
        end
      rescue StandardError
        flash.now[:danger] = @book_publisher.errors.full_messages.to_s
        render new_book_publisher_path, status: :not_acceptable
      end
    end
  
    def index
      @book_publisher = BookPublisher.all
    end
  
    def show
      @book_publisher = BookPublisher.find(params[:id])
    end
  
    def update
      @book_publisher = BookPublisher.find(params[:id])
      begin
        if @book_publisher.update!(bookp_params)
          redirect_to action: :index
          flash[:success] = 'BookPublisher has been updated successfully 🥳 ... '
        end
      rescue StandardError
        flash.now[:danger] = @book_publisher.errors.full_messages.to_s
        render action: 'edit'
      end
    end
  
    def edit
      @book_publisher = BookPublisher.find(params[:id])
    end
  
    def destroy
      BookPublisher.find(params[:id]).destroy
      redirect_to action: :index
      flash[:success] = 'BookPublisher has been deleted successfully 🥳 ... '
    end

    private
    def bookp_params
      params.require(:book_publisher).permit(:pub_date, :publisher_id, :book_id)
    end
end
