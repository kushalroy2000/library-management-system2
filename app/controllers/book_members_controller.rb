class BookMembersController < ApplicationController
    def new
        @books = Book.all
        @members = Member.all
    end

    def create
      @book_member = BookMember.new(bookm_params)
      begin
        if @book_member.save!
          redirect_to action: :index
          flash[:success] = 'BookMember has been saved successfully 🥳 ... '
        end
      rescue StandardError
        flash.now[:danger] = @book_member.errors.full_messages.to_s
        render new_book_member_path, status: :not_acceptable
      end
    end
  
    def index
      @book_member = BookMember.all
    end
  
    def show
      @book_member = BookMember.find(params[:id])
    end
  
    def update
      @book_member = BookMember.find(params[:id])
      begin
        if @book_member.update!(bookm_params)
          redirect_to action: :index
          flash[:success] = 'BookMember has been updated successfully 🥳 ... '
        end
      rescue StandardError
        flash.now[:danger] = @book_member.errors.full_messages.to_s
        render action: 'edit'
      end
    end
  
    def edit
      @book_member = BookMember.find(params[:id])
    end
  
    def destroy
      BookMember.find(params[:id]).destroy
      redirect_to action: :index
      flash[:success] = 'BookMember has been deleted successfully 🥳 ... '
    end

    private
    def bookm_params
      params.require(:book_member).permit(:book_id, :member_id, :issue_date, :due_date, :return_date)
    end
end
