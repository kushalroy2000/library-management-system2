# frozen_string_literal: true

class BooksController < ApplicationController
  def new; end

  def create
    @book = Book.new(book_params)
    begin
      if @book.save!
        redirect_to action: :index
        flash[:success] = 'Book has been saved successfully 🥳 ... '
      end
    rescue StandardError
      flash.now[:danger] = @book.errors.full_messages.to_s
      render new_book_path, status: :not_acceptable
    end
  end

  def index
    @book = Book.all
  end

  def show
    @book = Book.find(params[:id])
  end

  def update
    @book = Book.find(params[:id])
    begin
      if @book.update!(book_params)
        redirect_to action: :index
        flash[:success] = 'Book has been updated successfully 🥳 ... '
      end
    rescue StandardError
      flash.now[:danger] = @book.errors.full_messages.to_s
      render action: 'edit'
    end
  end

  def edit
    @book = Book.find(params[:id])
  end

  def destroy
    Book.find(params[:id]).destroy
    redirect_to action: :index
    flash[:success] = 'Book has been deleted successfully 🥳 ... '
  end

  private

  def book_params
    params.require(:book).permit(:author, :title, :price, :availability)
  end
end
