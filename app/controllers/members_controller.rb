# frozen_string_literal: true

class MembersController < ApplicationController
  def new; end

  def create
    @member = Member.new(member_params)
    begin
      if @member.save!
        redirect_to action: :index
        flash[:success] = 'Member has been saved successfully 🥳 ... '
      end
    rescue StandardError
      flash.now[:danger] = @member.errors.full_messages.to_s
      render new_member_path, status: :not_acceptable
    end
  end

  def index
    @member = Member.all
  end

  def show
    @member = Member.find(params[:id])
  end

  def update
    @member = Member.find(params[:id])
    begin
      if @member.update!(member_params)
        redirect_to action: :index
        flash[:success] = 'Member has been updated successfully 🥳 ... '
      end
    rescue StandardError
      flash.now[:danger] = @member.errors.full_messages.to_s
      render action: 'edit'
    end
  end

  def edit
    @member = Member.find(params[:id])
  end

  def destroy
    Member.find(params[:id]).destroy
    redirect_to action: :index
    flash[:success] = 'Member has been deleted successfully 🥳 ... '
  end

  private

  def member_params
    params.require(:member).permit(:memb_info, :memb_type, :address, :attriName, :expiry_date)
  end
end
